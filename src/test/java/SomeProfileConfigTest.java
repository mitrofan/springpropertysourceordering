import components.ManualBean;
import initializer.ExternalConfigurationInitializer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@ActiveProfiles("someProfile")
@ContextConfiguration(
    locations = "/configuration.xml",
    initializers = ExternalConfigurationInitializer.class)
public class SomeProfileConfigTest {
  @Autowired private ManualBean manualBean;

  @Autowired private ApplicationContext applicationContext;

  @Test
  public void testManualBeanPropInjected() {
    assertEquals("testPropertyValue", manualBean.getProp());
  }

  @Test
  public void testManualBeanProfileDependentPropEqualsDefaultValue() {
    assertEquals("overriddenInSomeProfileValue", manualBean.getProfileDependingProp());
  }

  @Test
  public void testOnlyInSomeProfileBeanExists() {
    assertTrue(applicationContext.containsBean("onlyInSomeProfile"));
  }
}
