import components.ManualBean;
import initializer.ExternalConfigurationInitializer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@TestPropertySource(properties = {"extConfig=classpath:/extConfig.properties"})
@ContextConfiguration(
    locations = "/configuration.xml",
    initializers = ExternalConfigurationInitializer.class)
public class ExternalConfigTest {
  @Autowired private ManualBean manualBean;

  @Autowired private ApplicationContext applicationContext;

  @Test
  public void testManualBeanPropInjected() {
    assertEquals("testPropertyValue", manualBean.getProp());
  }

  @Test
  public void testManualBeanProfileDependentProp() {
    assertEquals("overriddenInExtConfig", manualBean.getProfileDependingProp());
  }

  @Test
  public void testSomeProfileEnabledViaExtConfig() {
    assertTrue(applicationContext.getEnvironment().acceptsProfiles("someProfile"));
  }
}
