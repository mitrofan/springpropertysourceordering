import components.ManualBean;
import initializer.ExternalConfigurationInitializer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@ContextConfiguration(
    locations = "/configuration.xml",
    initializers = ExternalConfigurationInitializer.class)
public class DefaultConfigTest {
  @Autowired private ManualBean manualBean;

  @Test
  public void testManualBeanPropInjected() {
    assertEquals("testPropertyValue", manualBean.getProp());
  }

  @Test
  public void testManualBeanProfileDependentPropEqualsDefaultValue() {
    assertEquals("defaultValue", manualBean.getProfileDependingProp());
  }
}
