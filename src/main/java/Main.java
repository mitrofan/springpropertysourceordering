import components.Bean1;
import components.ManualBean;
import components.OnlyInSomeProfile;
import initializer.ExternalConfigurationInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/** Created by Dmitriy Chuchva on 9/9/2019. */
public class Main {
  public static void main(String[] args) {
    ConfigurableApplicationContext context =
        new ClassPathXmlApplicationContext(new String[] {"configuration.xml"}, false);
    new ExternalConfigurationInitializer().initialize(context);
    context.refresh();

    ManualBean manualBean = context.getBean(ManualBean.class);
    System.out.println("This is the instance of manual bean: " + manualBean.toString());
    System.out.println(manualBean.sayHello());

    Bean1 bean1 = context.getBean(Bean1.class);
    System.out.println(bean1.sayHello());

    if (context.containsBean("onlyInSomeProfile")) {
      OnlyInSomeProfile onlyInSomeProfile = context.getBean(OnlyInSomeProfile.class);
      System.out.println(
          "...And yes, we have onlyInSomeProfile bean in context! Here it's instance: "
              + onlyInSomeProfile);
    }
  }
}
