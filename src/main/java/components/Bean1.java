package components;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/** Created by Dmitriy Chuchva on 9/9/2019. */
@Component
public class Bean1 {
  private final Bean2 bean2;

  @Autowired
  public Bean1(Bean2 bean2) {
    this.bean2 = bean2;
  }

  public String sayHello() {
    return "Hello from components.Bean1. I have components.Bean2 instance: " + bean2.toString();
  }
}
