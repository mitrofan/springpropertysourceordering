package components;

import org.springframework.beans.factory.annotation.Autowired;

/** Created by Dmitriy Chuchva on 9/9/2019. */
public class ManualBean {
  private final Bean2 bean2;
  private String prop;
  private String profileDependingProp;

  @Autowired
  public ManualBean(Bean2 bean2) {
    this.bean2 = bean2;
  }

  public void setProp(String prop) {
    this.prop = prop;
  }

  public String getProp() {
    return prop;
  }

  public String getProfileDependingProp() {
    return profileDependingProp;
  }

  public String sayHello() {
    return "Hello from ManualBean! I have an instance of Bean2: "
        + bean2.toString()
        + " and a prop: "
        + prop
        + ", and profile-dependent prop: "
        + profileDependingProp;
  }

  public void setProfileDependingProp(String profileDependingProp) {
    this.profileDependingProp = profileDependingProp;
  }
}
