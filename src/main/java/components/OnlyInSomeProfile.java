package components;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("someProfile")
public class OnlyInSomeProfile {
}
