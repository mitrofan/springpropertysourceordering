package components.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.annotation.Order;

@Configuration
@Order(0)
@PropertySource("classpath:config.properties")
public class BaseConfiguration {}
