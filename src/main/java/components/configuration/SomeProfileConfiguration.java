package components.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.annotation.Order;

@Configuration
@Order(1)
@Profile("someProfile")
@PropertySource("classpath:/someProfile/config.properties")
public class SomeProfileConfiguration {}
