package web;

import components.ManualBean;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class MainController implements ApplicationContextAware {
  private final ManualBean manualBean;
  private ApplicationContext applicationContext;

  @Autowired
  public MainController(ManualBean manualBean) {
    this.manualBean = manualBean;
  }

  @Override
  public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
    this.applicationContext = applicationContext;
  }

  @GetMapping
  public String main() {
    return manualBean.sayHello();
  }

  @GetMapping("/profiles")
  public String profiles() {
    return String.join(",", applicationContext.getEnvironment().getActiveProfiles());
  }
}
