package initializer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.io.support.ResourcePropertySource;

import java.io.IOException;

public class ExternalConfigurationInitializer
    implements ApplicationContextInitializer<ConfigurableApplicationContext> {

  private final Log log = LogFactory.getLog(this.getClass());

  @Override
  public void initialize(ConfigurableApplicationContext applicationContext) {
    if (applicationContext.getEnvironment().containsProperty("extConfig")) {
      // legacy external config should override new profile-based configuration.
      // this could be done by adding one more @Configuration class with @PropertySource and highest
      // @Order, but in order to simplify migration the active profile will need to be specified not
      // via command line, but in external properties instead. This means that the external
      // properties should be added first programmatically before context refresh
      try {
        applicationContext
            .getEnvironment()
            .getPropertySources()
            .addFirst(
                new ResourcePropertySource(
                    applicationContext.getEnvironment().getProperty("extConfig")));
      } catch (IOException e) {
        // ignore, but not let it out
        log.warn("External configuration file has been specified but we were unable to load it", e);
      }
    }
  }
}
